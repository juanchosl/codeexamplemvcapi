<?php

namespace CodeExampleMVCApi\Action;

use CodeExampleMVCApi\Action\Action;
use CodeExampleMVCApi\Model\User;
use Psr\Http\Message\ResponseInterface as Response;
use Psr\Http\Message\ServerRequestInterface as Request;
use CodeExampleMVCApi\Exception\NotFoundException;
use CodeExampleMVCApi\Exception\PreconditionRequiredException;
use CodeExampleMVCApi\Exception\NotModifiedException;

class UserUpdateAction extends Action
{

    public function __invoke(Request $request, Response $response, array $args)
    {
        $body = $request->getParsedBody();
        if (empty($body)) {
            throw new PreconditionRequiredException("You need to send the fields to update");
        }

        try {
            $user = User::findOrFail($args['userid']);
        } catch (\Exception $ex) {
            throw new NotFoundException("The element with ID: {$args['userid']} Not exists");
//            return $this->response($response, ['error' => $ex->getMessage()], 404);
        }

        foreach ($body as $parameter => $value) {
            $user->{$parameter} = $value;
        }
        if ($user->save()) {
            return $this->response($response, $user, 200);
        } else {
            throw new NotModifiedException('Not modified');
//            return $this->response($response, ['error' => 'Not modified'], 304);
        }
    }

}
