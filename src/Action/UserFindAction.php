<?php

namespace CodeExampleMVCApi\Action;

use CodeExampleMVCApi\Action\Action;
use CodeExampleMVCApi\Model\User;
use Psr\Http\Message\ResponseInterface as Response;
use Psr\Http\Message\ServerRequestInterface as Request;
use CodeExampleMVCApi\Exception\NotFoundException;

class UserFindAction extends Action
{

    public function __invoke(Request $request, Response $response, array $args)
    {
        try {
            $user = User::findOrFail($args['userid']);
            return $this->response($response, $user);
        } catch (\Exception $ex) {
            throw new NotFoundException("The element with ID: {$args['userid']} Not exists");
//            return $this->response($response, ['error' => $ex->getMessage()], 404);
        }
    }

}
