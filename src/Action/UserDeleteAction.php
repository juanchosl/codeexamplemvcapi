<?php

namespace CodeExampleMVCApi\Action;

use CodeExampleMVCApi\Action\Action;
use CodeExampleMVCApi\Model\User;
use Psr\Http\Message\ResponseInterface as Response;
use Psr\Http\Message\ServerRequestInterface as Request;
use CodeExampleMVCApi\Exception\NotFoundException;
use CodeExampleMVCApi\Exception\NotModifiedException;

class UserDeleteAction extends Action
{

    public function __invoke(Request $request, Response $response, array $args)
    {
        try {
            $user = User::findOrFail($args['userid']);
        } catch (\Exception $ex) {
            throw new NotFoundException("The element with ID: {$args['userid']} Not exists");
        } finally {
            if ($user->delete()) {
                return $this->response($response, null, 205);
            } else {
                throw new NotModifiedException("The element with ID: {$args['userid']} can not be deleted");
            }
        }
    }

}
