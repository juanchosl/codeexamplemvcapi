<?php

namespace CodeExampleMVCApi\Action;

use CodeExampleMVCApi\Action\Action;
use CodeExampleMVCApi\Model\User;
use Psr\Http\Message\ResponseInterface as Response;
use Psr\Http\Message\ServerRequestInterface as Request;
use CodeExampleMVCApi\Exception\ServiceUnavailableException;
use CodeExampleMVCApi\Exception\NotModifiedException;
use CodeExampleMVCApi\Exception\PreconditionRequiredException;

class UserCreateAction extends Action
{

    public function __invoke(Request $request, Response $response, array $args)
    {
        $body = $request->getParsedBody();
        if (empty($body)) {
            throw new PreconditionRequiredException("You need to send the fields to insert");
        }

        $user = new User();
        $user->id = uniqid();
        $user->name = $body['name'];
        $user->age = $body['age'];
        try {
            $saved = $user->save();
        } catch (\Exception $ex) {
            $saved = false;
        } finally {
            if ($saved) {
                return $this->response($response, $user, 201);
            } else {
                throw new ServiceUnavailableException();
            }
        }
    }

}
