<?php

namespace CodeExampleMVCApi\Action;

class Action {

    public function response($response, $body, $code = 200)
    {
        $response = $response->withStatus($code);
        $response = $response->withHeader("Content-Type", "application/json");
        $response->getBody()->write(json_encode($body));

        return $response;
    }

}
