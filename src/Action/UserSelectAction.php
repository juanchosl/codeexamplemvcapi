<?php

namespace CodeExampleMVCApi\Action;

use CodeExampleMVCApi\Action\Action;
use CodeExampleMVCApi\Model\User;
use Psr\Http\Message\ResponseInterface as Response;
use Psr\Http\Message\ServerRequestInterface as Request;
use CodeExampleMVCApi\Exception\RangeNotSatisfiableException;

class UserSelectAction extends Action
{

    public function __invoke(Request $request, Response $response, array $args)
    {
        $query_filters = $request->getQueryParam('filters', []);
        $filters = [];
        foreach ($query_filters as $field => $value) {
            if (in_array($field, ['age', 'name'])) {
                $filters[] = [$field, '=', $value];
            }
        }
        $take = max(1, $request->getQueryParam('offset', 15));
        $skip = (max(1, $request->getQueryParam('page', 1)) - 1) * $take;
        $users = User::where($filters)
                ->take($take)
                ->skip($skip)
                ->get();
        if ($users->count() == 0) {
            throw new RangeNotSatisfiableException;
        }
        return $this->response($response, $users);
    }

}
