<?php

$mysql_connection = [
    "driver" => $_ENV['MYSQL_DRIVER'] ?? '',
    "host" => $_ENV['MYSQL_HOST'] ?? '',
    "database" => $_ENV['MYSQL_DATABASE'] ?? '',
    "username" => $_ENV['MYSQL_USER'] ?? '',
    "password" => $_ENV['MYSQL_PASSWORD'] ?? ''
];

defined('CONFIG_DATABASE_DEFAULT') or define('CONFIG_DATABASE_DEFAULT', $mysql_connection);
if (array_key_exists("DOCKER_HOST", $_SERVER)) {
    $host = $_SERVER['DOCKER_HOST'];
    preg_match('#(://?)([^:]+)(:?)#', $host, $match);
    $host = $match[2];
} elseif (array_key_exists("HTTP_HOST", $_SERVER)) {
    $host = $_SERVER['HTTP_HOST'];
} else {
    $host = 'webserver';
}
defined('CONFIG_URL_DOMAIN') or define('CONFIG_URL_DOMAIN', $host);
