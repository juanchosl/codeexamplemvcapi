<?php

namespace CodeExampleMVCApi\Exception;

class PreconditionRequiredException extends \Exception
{

    const CODE = 428;
    const MESSAGE = "Some preconditions are required";

    public function __construct(string $message = self::MESSAGE)
    {
        parent::__construct($message, self::CODE, null);
    }
}
