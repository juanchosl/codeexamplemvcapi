<?php

namespace CodeExampleMVCApi\Exception;

class ExpectationFailedException extends \Exception
{

    const CODE = 412;
    const MESSAGE = "Some expectation failed";

    public function __construct(string $message = self::MESSAGE)
    {
        parent::__construct($message, self::CODE, null);
    }

}
