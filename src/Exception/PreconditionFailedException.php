<?php

namespace CodeExampleMVCApi\Exception;

class PreconditionFailedException extends \Exception
{

    const CODE = 412;
    const MESSAGE = "Some preconditions has been failed";

    public function __construct(string $message = self::MESSAGE)
    {
        parent::__construct($message, self::CODE, null);
    }

}
