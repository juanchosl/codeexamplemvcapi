<?php

namespace CodeExampleMVCApi\Exception;

class RangeNotSatisfiableException extends \Exception
{

    const CODE = 416;
    const MESSAGE = "No results for the selected filters";

    public function __construct(string $message = self::MESSAGE)
    {
        parent::__construct($message, self::CODE, null);
    }

}
