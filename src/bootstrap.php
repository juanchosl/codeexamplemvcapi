<?php

require __DIR__ . DIRECTORY_SEPARATOR . ".." . DIRECTORY_SEPARATOR . "vendor" . DIRECTORY_SEPARATOR . "autoload.php";
require __DIR__ . DIRECTORY_SEPARATOR . "config.php";

use Illuminate\Database\Capsule\Manager as Capsule;

$capsule = new Capsule;
$capsule->addConnection(CONFIG_DATABASE_DEFAULT);
$capsule->setAsGlobal();
$capsule->bootEloquent();
