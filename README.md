## Developer
Juan Sánchez Lecegui <JuanchoSL@hotmail.com>

## Descripción
El presente proyecto crea y habilita una api para poder gestionar los usuarios de una base de datos. Está diseñado bajo patrón MVC

Tenemos los siguientes endpoints disponibles

* GET {host}/user -> para recuperar los users
* GET {host}/user/{userid} -> para recuperar 1 sólo user
* POST {host}/user -> para crear 1 user pasando los valores **name** y **age**
* PATCH {host}/user/{userid} -> para editar 1 user pasando los nuevos valores de **name** o **age**
* DELETE {host}/user/{userid} -> para eliminar 1 user

## Sistema
El proceso está creado y probado usando PHP-FPM 8.0 bajo nginx y MySQL 5.7

## Dependencias
Este proyecto usa framework Slim para la gestión de las Request y el ORM Eloquent para trabajar con la base de datos.

## Instalación
Instalar composer para instalar las dependencias y activar autoload

```
composer install
```

## Ejecución
Arrancar docker, ir al directorio del proyecto y activar las imágenes
```
docker-compose up -d
```

Una vez funcionando podremos acceder a la IP de nuestra máquina docker y lanzar las peticiones

## Documentación
Una vez corriendo los servicios, podemos acceder a **{host}/index.html** y tendremos disponible la documentación creada con Swagger y OpenApi

## Tests

Para ejecutar los tests, debemos entrar a nuestra máquina docker y: 
- lanzar __phpunit__ desde nuestra propia instalación, carpeta *tests*
- lanzar __phpunit__ desde el vendor
- usar los scripts agregados mediante __composer__

### Tests unitarios

```
vendor/bin/phpunit --bootstrap src/config.php tests/Unit
```
```
composer tests_unit
```

### Tests de integración

```
vendor/bin/phpunit --bootstrap src/config.php tests/Integration
```
```
composer tests_integration
```

### Todos los tests

```
vendor/bin/phpunit --bootstrap src/config.php tests
```
```
composer tests
```

