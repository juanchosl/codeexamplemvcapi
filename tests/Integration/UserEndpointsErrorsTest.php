<?php

namespace CodeExampleMVCApi\Tests\Integration;

class UserEndpointsErrorsTest extends Endpoints
{

    public function testUserNotExists()
    {
        return $this->remote("{$this->url}/user/100", 'GET', [], 404);
    }

    public function testCreateVoidUser()
    {
        $this->remote("{$this->url}/user", 'POST', [], 428);
    }

    public function testSelectNotRangedUser()
    {
        $this->remote("{$this->url}/user", 'GET', ['filters' => ['age' => 1000]], 416);
    }

}
