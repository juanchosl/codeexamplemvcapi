<?php

namespace CodeExampleMVCApi\Tests\Integration;

class UserEndpointsTest extends Endpoints
{

    protected function find($id)
    {
        return $this->remote("{$this->url}/user/{$id}", 'GET', [], 200);
    }

    protected function delete($id)
    {
        $this->remote("{$this->url}/user/{$id}", 'DELETE', [], 205);
    }

    public function testReadOneUser()
    {
        $element = $this->find('603bb3d819c20');
        $this->checkElement($element);
    }

    public function testReadSomeUsers()
    {
        $limits = [1, 2];
        foreach ($limits as $limit) {
            $response = $this->remote("{$this->url}/user", 'GET', ['offset' => $limit]);
            $this->assertTrue(is_array($response));
            $this->assertEquals($limit, count($response));
            foreach ($response as $element) {
                $this->checkElement($element);
            }
        }
    }

    public function testReadSomeUsersUsingFilters()
    {
        $response = $this->remote("{$this->url}/user", 'GET', ['filters' => ['age' => 20]]);
        $this->assertTrue(is_array($response));
        foreach ($response as $element) {
            $this->checkElement($element);
        }
    }

    public function testCreateAndUpdateAndDeleteAnUser()
    {
        $element = $this->remote("{$this->url}/user", 'POST', ['name' => 'Creation name', 'age' => 40], 201);
        $this->checkElement($element);

        $new_age = 30;
        $element = $this->remote("{$this->url}/user/{$element->id}", 'PATCH', ['age' => $new_age], 200);
        $this->checkElement($element);
        $this->assertEquals($new_age, $element->age);

        $this->delete($element->id);
    }

}
